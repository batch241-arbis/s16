//alert("Hello");

//Arithmentic Operators
let x = 1397;
let y = 7831;

//Addition Operator
let sum = x + y;
console.log("Result of the addition operator: " + sum);

//Subtraction Operator
let difference = x - y;
console.log("Result of the subtraction operator: " + difference);

//Multiplication Operator
let product = x * y;
console.log("Result of the multiplication operator: " + product);

//Division Operator
let quotient = x / y;
console.log("Result of the division operator: " + quotient);

//Modulo Operator
let remainder = y % x
console.log("Result of the modulo operator: " + remainder);

//Assignment Operator
//Basic Assignment Operator(=)
//The assignment operator adds the value of the right operand to a variable and assigns the result to the variable
let assignmentNumber = 8;

//Addition Assignment Operator
//Uses the current value of the variable and it ADDS a number (2) to itself. Afterwards, reassign it as a new value.
//assignmentNumber = assignmentNumber + 2;
assignmentNumber += 2;
console.log("Result of the addition assignment operator: " + assignmentNumber); //10


//Subtraction/Multiplication/Division (-=, *=, /=)

//Subtraction Assignment Operator
assignmentNumber -= 2; //assignmentNumber = assignmentNumber - 2;
console.log("Result of the subtraction assignment operator: " + assignmentNumber); //8

//Multiplication Assignment Operator
assignmentNumber *= 2; //assignmentNumber = assignmentNumber - 2;
console.log("Result of the multiplication assignment operator: " + assignmentNumber); //16

//Division Assignment Operator
assignmentNumber /= 2; //assignmentNumber = assignmentNumber - 2;
console.log("Result of the division assignment operator: " + assignmentNumber); //8


//Multiple Operators and Parenthesis
/*
	When we have multiple operators are applied in a single statement, it follows the PEMDAS Rule (Parenthesis, Exponents, Multiplication, Division, Addition, Subtraction)
	1. 3 * 4 = 12
	2. 12 / 5 = 2.4
	3. 1 + 2 = 3
	4. 3 - 2.4 = 0.6
*/
//let mdas = 3 - 2.4;
let mdas = 1 + 2 - 3 * 4 / 5
console.log("The result of mdas operation: " + mdas); //0.6000000000000001

// The order of operations can be changed by adding parenthesis to the logic
let pemdas = 1 + (2 - 3) * (4 / 5);
console.log("The result of pemdas operation: " + pemdas); //0.19999999999999996


//Increment and decrement
//Operators that add or subtract values by 1 and reassigns the value of the variable where the increment or decrement was applied to

let z = 1;

//Pre-Increment
// let increment = ++z;
// console.log("Result of the pre-increment: " + increment); //2

// console.log("Result of the pre-increment: " + z); //2

//Post-Increment 
//The value of "z" is returned and stored in the variable "postIncrement" then the value of "z" is increased by one
postIncrement = z++;
console.log("Result of the pre-increment: " + postIncrement); //2

console.log("Result of the pre-increment: " + z); //2

/*
	Pre-increment - adds 1 first before reading value
	Post-increment - reads value first before adding 1

	Pre-decrement - subtracts 1 first before reading value
	Post-decrement - reads value first before subtracting 1
*/

let a = 2;

//Pre-Decrement
//The value "a" is decreased by a value of 1 before returning the value and storing it in the variable "preDecrement";
// console.log("Result of the pre-decrement: " + preDecrement); 
// console.log("Result of the pre-decrement: " + a); 

//Post-Decrement
//The value "a" is returned and stored in the value in the variable "postDecrement" then the value of "a" is decreased by 1;
let postDecrement = a--;
console.log("Result of the post-decrement: " + postDecrement); 
console.log("Result of the post-decrement: " + a); 


//Type coercion
/*
	Type coercion is the automatic or implicit conversion of values from one data type to another
*/

let numA = "10"; //String
let numB = 12; //Number

/*
	Adding/concatenating a string and a number will result as a string
*/

let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion);

let coercion1 = numA - numB;
console.log(coercion1);
console.log(typeof coercion1);

//Non-coercion
let numC = 16;
let numD = 14;

let nonCoercion = numC + numD;
console.log(nonCoercion);
console.log(typeof nonCoercion);


//Addition of Number and Boolean
/*
	The result is a number
	The boolean "true" is associated with the value of 1
	The boolean "false" is associated with the value of 0
*/

let numE = true + 1;
console.log(numE);
console.log(typeof numE);

let numF = false + 1;
console.log(numF);


//Comparison Operator
let juan = "juan";

//Equality Operator(==)
/*
	-Checks whether the operands are equal/have the same content
	-Attempts to CONVERT and COMPARE operands of the different data types
	-Returns a boolean value (true / false)
*/

console.log(1 == 1);
console.log(1 == 2);
console.log(1 == "1");
console.log(1 == true);
//compare two strings that are the same
console.log ("juan" == "juan");
console.log ("true" == true);
console.log(juan == "juan");

//Inequality Operator(!=)
/*
	-Checks whether the operands are not equal/have different content
	-Attempts to CONVERT and COMPARE operands of different data types
*/

console.log(1 != 1);
console.log(1 != 2);
console.log(1 != "1");
console.log(1 != true);
console.log ("juan" != "juan");
console.log(juan != "juan");


//Strict Equality Operator(===)
/*
	-Check whether the operands are equal/have the same content
	-Also COMPARES the data types of 2 values
*/

console.log(1 === 1);
console.log(1 === 2);
console.log(1 === "1");
console.log(1 === true);
console.log ("juan" === "juan");
console.log(juan === "juan");

//Strict Inquality Operator(!==)
/*
	-Check whether the operands are not equal/do not have the same content
	-Also COMPARES the data types of 2 values
*/

console.log(1 !== 1);
console.log(1 !== 2);
console.log(1 !== "1");
console.log(1 !== true);
console.log ("juan" !== "juan");
console.log(juan !== "juan");


//Relational Operator
//Returns a Boolean value
let j = 50;
let k = 65;

//GT/Greater than Operator (>)
let isGreaterThan = j > k;
console.log(isGreaterThan);
//LT/Less than Operator (<)
let isLessThan = j < k;
console.log(isLessThan);

//GT/Greater than or Equal Operator (>=)
let isGTorEqual = j >= k;
console.log(isGTorEqual);

//LT/Less than or Equal Operator (<=)
let isLTorEqual = j <= k;
console.log(isLTorEqual);


//Logical Operators
let isLegalAge = true;
let isRegistered = false;

//Logical AND Operator (&& - Double Ampersand)
//Returns true if all operands are true
//true && true = true
//true && false = false

let allRequirementsMet = isLegalAge && isRegistered
console.log(allRequirementsMet);

//Logical OR Operator (|| - Double Pipe)
// Returns true if one of the operands are true
// true || true = true
// true || false = true
// false || false = false
let someRequirementsMet = isLegalAge || isRegistered;
console.log(someRequirementsMet);

//Logical NOT Operator (! - Exclamation point)
let someRequirementsNotMet = !isRegistered;
console.log(someRequirementsNotMet);